import Flutter
import UIKit

public class SwiftAppIconPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "app_icon", binaryMessenger: registrar.messenger())
    let instance = SwiftAppIconPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    receiveAppIconBytes(result: result);
  }

      private func receiveAppIconBytes(result: FlutterResult) {
        var appIcon: UIImage! {
             guard let iconsDictionary = Bundle.main.infoDictionary?["CFBundleIcons"] as? [String:Any],
             let primaryIconsDictionary = iconsDictionary["CFBundlePrimaryIcon"] as? [String:Any],
             let iconFiles = primaryIconsDictionary["CFBundleIconFiles"] as? [String],
             let lastIcon = iconFiles.last else { return nil }
             return UIImage(named: lastIcon)
           }
        if(appIcon != nil) {
            result(appIcon.pngData());
        } else {
            result(FlutterError(code: "UNAVAILABLE",
                                message: "AppIcon unavailable",
                                details: nil));
        }
    }
}
