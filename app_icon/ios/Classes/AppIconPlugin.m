#import "AppIconPlugin.h"
#if __has_include(<app_icon/app_icon-Swift.h>)
#import <app_icon/app_icon-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "app_icon-Swift.h"
#endif

@implementation AppIconPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAppIconPlugin registerWithRegistrar:registrar];
}
@end
