import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class AppIcon {
  static const MethodChannel _channel = const MethodChannel('app_icon');
  static ImageProvider _provider;

  static Future<Uint8List> get appIconByteArray async {
    return _channel.invokeMethod('getApplicationIcon');
  }

  static Future<ImageProvider> get appIconImageProvider async {
    if (!(_provider is ImageProvider)) {
      final Uint8List bytes = await appIconByteArray;
      _provider = MemoryImage(bytes ?? []);
    }
    return _provider;
  }

  static ImageProvider get iconImageProviderSync {
    return _provider;
  }
}

class AppIconWidget extends StatefulWidget {
  final double height;
  final double width;
  final BoxFit fit;

  const AppIconWidget({Key key, this.height, this.width, this.fit})
      : super(key: key);
  @override
  _AppIconWidgetState createState() => _AppIconWidgetState();
}

class _AppIconWidgetState extends State<AppIconWidget> {
  ImageProvider _logoProvider;

  @override
  void initState() {
    if (AppIcon.iconImageProviderSync is ImageProvider) {
      _logoProvider = AppIcon.iconImageProviderSync;
    }
    AppIcon.appIconImageProvider.then((value) {
      if (mounted)
        setState(() {
          _logoProvider = value;
        });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_logoProvider is ImageProvider) {
      return Image(
          image: _logoProvider,
          height: widget.height,
          width: widget.width,
          fit: widget.fit);
    }
    return SizedBox();
  }
}
